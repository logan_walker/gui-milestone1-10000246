﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task01_console
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine(q1);
                var number = Console.ReadLine();

                Console.WriteLine(q2);
                var result = int.Parse(Console.ReadLine());

                Console.WriteLine($"{choices(number, result)}");

                Console.WriteLine(q3);
                var num = int.Parse(Console.ReadLine());
                if (num == 0)
                {
                    Console.Clear();
                }
                else if (num == 1)
                {
                    Console.WriteLine(q4);
                    break;
                }
            }
        }

        static string q1 = "\nType in a number you want to convert:";
        static string q2 = "\nDo you want to convert it to [0]Miles or [1]Kilometers?";
        static string q3 = "\nDo you want to convert another number? [0] Yes [1] No";
        static string q4 = "\nThank you for using my distance converter! Have a nice day.";

        static string choices(string number, int choice)
        {
            var answer = "";

            switch (choice)
            {
                case 0:
                    answer = $"\n{number} Kilometers = {convertkm(double.Parse(number))} Miles";
                    break;
                case 1:
                    answer = $"\n{number} Miles = {convertmile(double.Parse(number))} Kilometers";
                    break;
                default:
                    answer = $"\nYou did not enter a valid value.";
                    break;
            }

            return answer;
        }

        static double convertmile(double input)
        {
            const double converter = 1.609344;
            var mile = Convert.ToDouble(input);
            var result = mile * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        static double convertkm(double input)
        {
            const double converter = 0.621371192;
            var km = Convert.ToDouble(input);
            var result = km * converter;

            result = System.Math.Round(result, 2);
            return result;
        }
    }
}
