﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string q1 = "Please type in the name of a fruit or vegetable and I will check whether or not we sell it.\n";

        public MainPage()
        {
            this.InitializeComponent();

            lblPrompt1.Text = q1;
        }

        static string fruitVegDictionary(string checkDictionary)
        {
            var yesNo = "";

            var stock = new Dictionary<string, string>();

            //Adding 10 veges to the dictionary
            stock.Add("potato", "vegetable");
            stock.Add("onion", "vegetable");
            stock.Add("carrot", "vegetable");
            stock.Add("cabbage", "vegetable");
            stock.Add("lettuce", "vegetable");
            stock.Add("broccoli", "vegetable");
            stock.Add("cauliflower", "vegetable");
            stock.Add("cucumber", "vegetable");
            stock.Add("beetroot", "vegetable");
            stock.Add("zucchini", "vegetable");
            //Also adding the plurals of the 10 veges
            stock.Add("potatoes", "vegetable");
            stock.Add("onions", "vegetable");
            stock.Add("carrots", "vegetable");
            stock.Add("cabbages", "vegetable");
            stock.Add("lettuces", "vegetable");
            stock.Add("cucumbers", "vegetable");
            stock.Add("beetroots", "vegetable");
            stock.Add("zucchinis", "vegetable");

            //Adding 10 fruit to the dictionary
            stock.Add("apple", "fruit");
            stock.Add("orange", "fruit");
            stock.Add("grape", "fruit");
            stock.Add("pear", "fruit");
            stock.Add("watermelon", "fruit");
            stock.Add("strawberry", "fruit");
            stock.Add("mandarin", "fruit");
            stock.Add("grapefruit", "fruit");
            stock.Add("papaya", "fruit");
            stock.Add("pitaya", "fruit");
            //Also adding the plurals of the 10 fruit
            stock.Add("apples", "fruit");
            stock.Add("oranges", "fruit");
            stock.Add("grapes", "fruit");
            stock.Add("pears", "fruit");
            stock.Add("watermelons", "fruit");
            stock.Add("strawberries", "fruit");
            stock.Add("mandarins", "fruit");
            stock.Add("grapefruits", "fruit");
            stock.Add("papayas", "fruit");
            stock.Add("pitayas", "fruit");

            if (stock.ContainsKey(checkDictionary))
            {
                yesNo = $"\nYes, we do indeed sell {checkDictionary}!";
            }
            else
            {
                yesNo = $"\nNo, i'm afraid we do not sell {checkDictionary}.";
            }

            return yesNo;
        }

        private void buttonCheck_Click(object sender, RoutedEventArgs e)
        {
            yesNo.Text = fruitVegDictionary(checkDictionary.Text);
            checkDictionary.Text = "";
        }
    }
}
