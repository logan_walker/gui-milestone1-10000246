﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task03_console
{
    class Program
    {
        static string q1 = "Please select which animal you would like to learn about.";
        static string q2 = "\n[0]Gorillas\n[1]Elephants\n[2]Lions\n[3]Giraffes\n[4]To exit the program";

        static string q3 = "\nCome back any time to learn more about these fascinating animals.";
        static string q4 = "\nNext time choose a valid option. Thank You.";

        static string q5 = "\nPress <Enter> to choose a different animal.";


        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine(q1);
                Console.WriteLine(q2);
                var userOptions = int.Parse(Console.ReadLine());
                if (userOptions < 4)
                {
                    Console.WriteLine($"\n{options(userOptions)}");
                    Console.WriteLine(q5);
                    Console.ReadKey();
                }
                else if (userOptions == 4)
                {
                    Console.WriteLine(q3);
                    Console.ReadKey();

                    break;
                }
                else
                {
                    Console.WriteLine(q4);
                    Console.ReadKey();

                    break;
                }
            }

        }

        static string options(int userOptions)
        {
            var result = "";

            switch (userOptions)
            {
                case 0:
                    result = "Harambe was a Gorilla.";
                    break;
                case 1:
                    result = "Elephants are really big and have big ears.";
                    break;
                case 2:
                    result = "Lions eat things regularly.";
                    break;
                case 3:
                    result = "Giraffes have pretty long necks.";
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
