﻿namespace Task05_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblRules1 = new System.Windows.Forms.Label();
            this.lblRules2 = new System.Windows.Forms.Label();
            this.lblRules3 = new System.Windows.Forms.Label();
            this.lblRules4 = new System.Windows.Forms.Label();
            this.lblStart = new System.Windows.Forms.Label();
            this.enterGuess = new System.Windows.Forms.TextBox();
            this.btnGuess = new System.Windows.Forms.Button();
            this.lblScore = new System.Windows.Forms.Label();
            this.lblTurnsRemaining = new System.Windows.Forms.Label();
            this.lblCorrect = new System.Windows.Forms.Label();
            this.lblResult1 = new System.Windows.Forms.Label();
            this.lblPlayAgain = new System.Windows.Forms.Label();
            this.lblRandGuess = new System.Windows.Forms.Label();
            this.btnYes = new System.Windows.Forms.Button();
            this.lblResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblRules1
            // 
            this.lblRules1.AutoSize = true;
            this.lblRules1.Location = new System.Drawing.Point(3, 5);
            this.lblRules1.Name = "lblRules1";
            this.lblRules1.Size = new System.Drawing.Size(0, 13);
            this.lblRules1.TabIndex = 0;
            // 
            // lblRules2
            // 
            this.lblRules2.AutoSize = true;
            this.lblRules2.Location = new System.Drawing.Point(3, 18);
            this.lblRules2.Name = "lblRules2";
            this.lblRules2.Size = new System.Drawing.Size(0, 13);
            this.lblRules2.TabIndex = 1;
            // 
            // lblRules3
            // 
            this.lblRules3.AutoSize = true;
            this.lblRules3.Location = new System.Drawing.Point(3, 31);
            this.lblRules3.Name = "lblRules3";
            this.lblRules3.Size = new System.Drawing.Size(0, 13);
            this.lblRules3.TabIndex = 2;
            // 
            // lblRules4
            // 
            this.lblRules4.AutoSize = true;
            this.lblRules4.Location = new System.Drawing.Point(3, 44);
            this.lblRules4.Name = "lblRules4";
            this.lblRules4.Size = new System.Drawing.Size(0, 13);
            this.lblRules4.TabIndex = 3;
            // 
            // lblStart
            // 
            this.lblStart.AutoSize = true;
            this.lblStart.Location = new System.Drawing.Point(3, 64);
            this.lblStart.Name = "lblStart";
            this.lblStart.Size = new System.Drawing.Size(0, 13);
            this.lblStart.TabIndex = 4;
            // 
            // enterGuess
            // 
            this.enterGuess.Location = new System.Drawing.Point(6, 97);
            this.enterGuess.Name = "enterGuess";
            this.enterGuess.Size = new System.Drawing.Size(74, 20);
            this.enterGuess.TabIndex = 5;
            // 
            // btnGuess
            // 
            this.btnGuess.Location = new System.Drawing.Point(5, 123);
            this.btnGuess.Name = "btnGuess";
            this.btnGuess.Size = new System.Drawing.Size(75, 23);
            this.btnGuess.TabIndex = 6;
            this.btnGuess.Text = "Guess";
            this.btnGuess.UseVisualStyleBackColor = true;
            this.btnGuess.Click += new System.EventHandler(this.btnGuess_Click);
            // 
            // lblScore
            // 
            this.lblScore.AutoSize = true;
            this.lblScore.Location = new System.Drawing.Point(101, 89);
            this.lblScore.Name = "lblScore";
            this.lblScore.Size = new System.Drawing.Size(0, 13);
            this.lblScore.TabIndex = 7;
            // 
            // lblTurnsRemaining
            // 
            this.lblTurnsRemaining.AutoSize = true;
            this.lblTurnsRemaining.Location = new System.Drawing.Point(101, 115);
            this.lblTurnsRemaining.Name = "lblTurnsRemaining";
            this.lblTurnsRemaining.Size = new System.Drawing.Size(0, 13);
            this.lblTurnsRemaining.TabIndex = 8;
            // 
            // lblCorrect
            // 
            this.lblCorrect.AutoSize = true;
            this.lblCorrect.Location = new System.Drawing.Point(5, 153);
            this.lblCorrect.Name = "lblCorrect";
            this.lblCorrect.Size = new System.Drawing.Size(0, 13);
            this.lblCorrect.TabIndex = 9;
            // 
            // lblResult1
            // 
            this.lblResult1.AutoSize = true;
            this.lblResult1.Location = new System.Drawing.Point(5, 213);
            this.lblResult1.Name = "lblResult1";
            this.lblResult1.Size = new System.Drawing.Size(0, 13);
            this.lblResult1.TabIndex = 10;
            // 
            // lblPlayAgain
            // 
            this.lblPlayAgain.AutoSize = true;
            this.lblPlayAgain.Location = new System.Drawing.Point(5, 243);
            this.lblPlayAgain.Name = "lblPlayAgain";
            this.lblPlayAgain.Size = new System.Drawing.Size(0, 13);
            this.lblPlayAgain.TabIndex = 11;
            // 
            // lblRandGuess
            // 
            this.lblRandGuess.AutoSize = true;
            this.lblRandGuess.Location = new System.Drawing.Point(5, 166);
            this.lblRandGuess.Name = "lblRandGuess";
            this.lblRandGuess.Size = new System.Drawing.Size(0, 13);
            this.lblRandGuess.TabIndex = 12;
            // 
            // btnYes
            // 
            this.btnYes.Location = new System.Drawing.Point(5, 259);
            this.btnYes.Name = "btnYes";
            this.btnYes.Size = new System.Drawing.Size(75, 23);
            this.btnYes.TabIndex = 13;
            this.btnYes.Text = "Restart";
            this.btnYes.UseVisualStyleBackColor = true;
            this.btnYes.Click += new System.EventHandler(this.btnYes_Click);
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Location = new System.Drawing.Point(5, 183);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(0, 13);
            this.lblResult.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 320);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.btnYes);
            this.Controls.Add(this.lblRandGuess);
            this.Controls.Add(this.lblPlayAgain);
            this.Controls.Add(this.lblResult1);
            this.Controls.Add(this.lblCorrect);
            this.Controls.Add(this.lblTurnsRemaining);
            this.Controls.Add(this.lblScore);
            this.Controls.Add(this.btnGuess);
            this.Controls.Add(this.enterGuess);
            this.Controls.Add(this.lblStart);
            this.Controls.Add(this.lblRules4);
            this.Controls.Add(this.lblRules3);
            this.Controls.Add(this.lblRules2);
            this.Controls.Add(this.lblRules1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblRules1;
        private System.Windows.Forms.Label lblRules2;
        private System.Windows.Forms.Label lblRules3;
        private System.Windows.Forms.Label lblRules4;
        private System.Windows.Forms.Label lblStart;
        private System.Windows.Forms.TextBox enterGuess;
        private System.Windows.Forms.Button btnGuess;
        private System.Windows.Forms.Label lblScore;
        private System.Windows.Forms.Label lblTurnsRemaining;
        private System.Windows.Forms.Label lblCorrect;
        private System.Windows.Forms.Label lblResult1;
        private System.Windows.Forms.Label lblPlayAgain;
        private System.Windows.Forms.Label lblRandGuess;
        private System.Windows.Forms.Button btnYes;
        private System.Windows.Forms.Label lblResult;
    }
}

