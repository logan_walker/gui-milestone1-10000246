﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task05_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string rules1 = "Rules:";
        static string rules2 = "I will choose a random number between 1 and 5 and you have to guess what it is! Simple, right?";
        static string rules3 = "If you guess correctly you will get a point. If you guess incorrectly you will not get any points.";
        static string rules4 = "You have 5 tries to get as many points as you can! Good luck!";
        static string start = "I have chosen a number between 1 and 5. Can you guess it?";
        static string correct = "Well done! You guessed correctly!";
        static string incorrect = "Bad luck. You didn't guess the right number.";
        static string invalid = "That is an invalid guess. You wasted a turn. Please guess a number between 1 and 5.";
        static string result = "That's terrible! You can do better!";
        static string result1 = "That's not bad! You can still do better!";
        static string result2 = "That's a perfect score! Well done!";
        static string playAgain = "Would you like restart?";

        static int guess = 0;
        static int number = 0;
        static int turns = 0;
        static int score = 0;
        //Changed default turnsRemaining to 4 because it doesn't first appear until after first guess is made so user will have 4 turns left at this point
        static int turnsRemaining = 4;

        public MainPage()
        {
            this.InitializeComponent();
            lblRules1.Text = rules1;
            lblRules2.Text = rules2;
            lblRules3.Text = rules3;
            lblRules4.Text = rules4;
            lblStart.Text = start;
            lblPlayAgain.Text = playAgain;
        }

        private void btnGuess_Click(object sender, RoutedEventArgs e)
        {
            game();
            enterGuess.Text = "";
        }

        private void btnYes_Click(object sender, RoutedEventArgs e)
        {
            guess = 0;
            number = 0;
            turns = 0;
            score = 0;
            turnsRemaining = 4;
            enterGuess.Text = "";
            lblCorrect.Text = "";
            lblRandGuess.Text = "";
            lblResult.Text = "";
            lblResult1.Text = "";
            lblScore.Text = "";
            lblTurnsRemaining.Text = "";
            btnGuess.IsEnabled = true;
        }

        //Method to generate a random number between 1 and 5 and return it
        public static int generateRandomNumber()
        {
            Random random = new Random();
            int number = random.Next(1, 6);

            return number;
        }

        //Method that contains the main game code
        public void game()
        {
            lblTurnsRemaining.Text = $"\nYou have {turnsRemaining} turns left.";

            int.TryParse(enterGuess.Text, out guess);
            number = generateRandomNumber();
            if (guess > 5 || guess < 1)
            {
                turnsRemaining = turnsRemaining - 1;
                lblScore.Text = $"\nYour current score is: {score}";
                lblCorrect.Text = invalid;
            }
            else if (guess == number)
            {
                turnsRemaining = turnsRemaining - 1;
                score = score + 1;
                lblScore.Text = $"\nYour current score is: {score}";
                lblRandGuess.Text = "";
                lblCorrect.Text = correct;
            }
            else if (guess != number && guess <= 5 && guess >= 1)
            {
                turnsRemaining = turnsRemaining - 1;
                lblScore.Text = $"\nYour current score is: {score}";
                lblCorrect.Text = incorrect;
                lblRandGuess.Text = $"I guessed the number {number}";
            }
            turns++;
            if (turns == 5)
            {
                btnGuess.IsEnabled = false;
                end();
            }
        }

        //Method that runs when the game ends. Displaying the final score
        //Also displays buttons which give the user the option to play again or exit
        public void end()
        {
            lblResult.Text = $"\nYour final score is: {score}";
            if (score == 0)
            {
                lblResult1.Text = result;
            }
            else if (score >= 1 && score <= 4)
            {
                lblResult1.Text = result1;
            }
            else if (score == 5)
            {
                lblResult1.Text = result2;
            }
        }
    }
}
