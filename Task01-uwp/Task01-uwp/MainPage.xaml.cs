﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            Question01.Text = q1;
            Question02.Text = q2;
            labelAnswer.Text = "";
        }

        static string q1 = $"Type in a number you want to convert:";
        static string q2 = $"Do you want to convert it to Miles or Kilometers?";

        static string choices(string number, int choice)
        {
            var answer = "";

            switch (choice)
            {
                case 0:
                    answer = $"{number} Kilometers = {convertkm(double.Parse(number))} Miles";
                    break;
                case 1:
                    answer = $"{number} Miles = {convertmile(double.Parse(number))} Kilometers";
                    break;
                default:
                    answer = $"Not a valid value was given";
                    break;
            }

            return answer;
        }
        static double convertmile(double input)
        {
            const double converter = 1.609344;
            var mile = Convert.ToDouble(input);
            var result = mile * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        static double convertkm(double input)
        {
            const double converter = 0.621371192;
            var km = Convert.ToDouble(input);
            var result = km * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            labelAnswer.Text = choices(textInput.Text, cmbSelection.SelectedIndex);
        }
    }
}
