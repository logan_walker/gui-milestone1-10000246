﻿namespace Task03_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPrompt1 = new System.Windows.Forms.Label();
            this.userOptions = new System.Windows.Forms.ComboBox();
            this.output = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPrompt1
            // 
            this.lblPrompt1.AutoSize = true;
            this.lblPrompt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrompt1.Location = new System.Drawing.Point(13, 13);
            this.lblPrompt1.Name = "lblPrompt1";
            this.lblPrompt1.Size = new System.Drawing.Size(41, 15);
            this.lblPrompt1.TabIndex = 0;
            this.lblPrompt1.Text = "label1";
            // 
            // userOptions
            // 
            this.userOptions.FormattingEnabled = true;
            this.userOptions.Items.AddRange(new object[] {
            "Gorillas",
            "Elephants",
            "Lions",
            "Giraffes"});
            this.userOptions.Location = new System.Drawing.Point(16, 42);
            this.userOptions.Name = "userOptions";
            this.userOptions.Size = new System.Drawing.Size(121, 21);
            this.userOptions.TabIndex = 1;
            this.userOptions.SelectedIndexChanged += new System.EventHandler(this.userOptions_SelectedIndexChanged);
            // 
            // output
            // 
            this.output.AutoSize = true;
            this.output.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.output.Location = new System.Drawing.Point(16, 77);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(117, 16);
            this.output.TabIndex = 2;
            this.output.Text = "Make your choice.";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 350);
            this.Controls.Add(this.output);
            this.Controls.Add(this.userOptions);
            this.Controls.Add(this.lblPrompt1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPrompt1;
        private System.Windows.Forms.ComboBox userOptions;
        private System.Windows.Forms.Label output;
    }
}

