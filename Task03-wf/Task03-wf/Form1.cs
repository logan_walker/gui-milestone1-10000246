﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task03_wf
{
    public partial class Form1 : Form
    {
        static string q1 = "Please select which animal you would like to learn about.";

        public Form1()
        {
            InitializeComponent();
            lblPrompt1.Text = q1;
        }

        private void userOptions_SelectedIndexChanged(object sender, EventArgs e)
        {
            output.Text = options(userOptions.SelectedIndex);
        }

        static string options(int userOptions)
        {
            var result = "";

            switch (userOptions)
            {
                case 0:
                    result = "Harambe was a Gorilla.";
                    break;
                case 1:
                    result = "Elephants are really big and have big ears.";
                    break;
                case 2:
                    result = "Lions eat things regularly.";
                    break;
                case 3:
                    result = "Giraffes have pretty long necks.";
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
