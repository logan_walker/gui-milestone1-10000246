﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task01_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            Question01.Text = q1;
            Question02.Text = q2;
        }

        static string q1 = $"Type in a number you want to convert:";
        static string q2 = $"Do you want to convert it to Miles or Kilometers?";

        static string choices(string number, int choice)
        {
            var answer = "";

            switch (choice)
            {
                case 0:
                    answer = $"{number} Kilometers = {convertkm(double.Parse(number))} Miles";
                    break;
                case 1:
                    answer = $"{number} Miles = {convertmile(double.Parse(number))} Kilometers";
                    break;
                default:
                    answer = $"Not a valid value was given";
                    break;
            }

            return answer;
        }
        static double convertmile(double input)
        {
            const double converter = 1.609344;
            var mile = Convert.ToDouble(input);
            var result = mile * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        static double convertkm(double input)
        {
            const double converter = 0.621371192;
            var km = Convert.ToDouble(input);
            var result = km * converter;

            result = System.Math.Round(result, 2);
            return result;
        }

        private void cmbSelection_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            labelAnswer.Text = choices(textInput.Text, cmbSelection.SelectedIndex);
        }
    }
}
