﻿namespace Task01_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbSelection = new System.Windows.Forms.ComboBox();
            this.labelAnswer = new System.Windows.Forms.Label();
            this.Question01 = new System.Windows.Forms.Label();
            this.Question02 = new System.Windows.Forms.Label();
            this.textInput = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cmbSelection
            // 
            this.cmbSelection.FormattingEnabled = true;
            this.cmbSelection.Items.AddRange(new object[] {
            "Miles",
            "Kilometers"});
            this.cmbSelection.Location = new System.Drawing.Point(26, 128);
            this.cmbSelection.Name = "cmbSelection";
            this.cmbSelection.Size = new System.Drawing.Size(169, 21);
            this.cmbSelection.TabIndex = 0;
            this.cmbSelection.SelectedIndexChanged += new System.EventHandler(this.cmbSelection_SelectedIndexChanged_1);
            // 
            // labelAnswer
            // 
            this.labelAnswer.AutoSize = true;
            this.labelAnswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAnswer.Location = new System.Drawing.Point(26, 169);
            this.labelAnswer.Name = "labelAnswer";
            this.labelAnswer.Size = new System.Drawing.Size(0, 16);
            this.labelAnswer.TabIndex = 1;
            // 
            // Question01
            // 
            this.Question01.AutoSize = true;
            this.Question01.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Question01.Location = new System.Drawing.Point(26, 26);
            this.Question01.Name = "Question01";
            this.Question01.Size = new System.Drawing.Size(45, 16);
            this.Question01.TabIndex = 2;
            this.Question01.Text = "label1";
            // 
            // Question02
            // 
            this.Question02.AutoSize = true;
            this.Question02.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Question02.Location = new System.Drawing.Point(26, 96);
            this.Question02.Name = "Question02";
            this.Question02.Size = new System.Drawing.Size(45, 16);
            this.Question02.TabIndex = 3;
            this.Question02.Text = "label1";
            // 
            // textInput
            // 
            this.textInput.Location = new System.Drawing.Point(26, 58);
            this.textInput.Name = "textInput";
            this.textInput.Size = new System.Drawing.Size(100, 20);
            this.textInput.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(377, 257);
            this.Controls.Add(this.textInput);
            this.Controls.Add(this.Question02);
            this.Controls.Add(this.Question01);
            this.Controls.Add(this.labelAnswer);
            this.Controls.Add(this.cmbSelection);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbSelection;
        private System.Windows.Forms.Label labelAnswer;
        private System.Windows.Forms.Label Question01;
        private System.Windows.Forms.Label Question02;
        private System.Windows.Forms.TextBox textInput;
    }
}

