﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task02_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            labelPrompt.Text = q1;
            labelPrompt2.Text = q2;
        }

        static double price = 0.00;
        static double totalprice = 0.00;

        static string q1 = $"Enter the price of the item in dollars:";
        static string q2 = $"\nClick 'Total' when finished to display your total price (inc GST).";

        static int choice = 0;

        private void buttonAddItem_Click(object sender, EventArgs e)
        {
            double.TryParse(enterPrice.Text, out price);
            result.Text = addtotalprice(price, choice);
            totalprice = System.Math.Round(totalprice, 2);
            currentTotal.Text = $"Your current total is:\n${totalprice}";
            enterPrice.Text = String.Empty;
        }

        private void no_Click(object sender, EventArgs e)
        {
            choice = 1;
            result.Text = addtotalprice(totalprice, choice);
            thankYou.Text = "Thank you for using my price checker! Have a nice day.";
            //Freezes both buttons when total (inc GST) is shown.
            no.Enabled = false;
            buttonAddItem.Enabled = false;
        }

        //Method for adding the total price and then displaying the total, depending on what option the user chooses
        static string addtotalprice(double price, int choice)
        {
            var result = "";

            switch (choice)
            {
                case 0:
                    totalprice = totalprice + price;
                    totalprice = System.Math.Round(totalprice, 2);
                    break;

                case 1:
                    //totalprice = totalprice + price;
                    totalprice = System.Math.Round(totalprice, 2);
                    result = $"The total price (including GST) is ${Gst(totalprice)}";
                    break;

                default:
                    result = $"Please give a valid price.";
                    break;
            }
            return result;

        }

        //Method for taking the total and then returning the total+GST
        static string Gst(double totalprice)
        {
            var output = "";
            totalprice = totalprice * 1.15;

            totalprice = System.Math.Round(totalprice, 2);
            output = Convert.ToString(totalprice);

            return output;
        }
    }
}
