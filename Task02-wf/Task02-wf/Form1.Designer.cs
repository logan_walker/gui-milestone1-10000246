﻿namespace Task02_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPrompt = new System.Windows.Forms.Label();
            this.buttonAddItem = new System.Windows.Forms.Button();
            this.result = new System.Windows.Forms.Label();
            this.labelPrompt2 = new System.Windows.Forms.Label();
            this.currentTotal = new System.Windows.Forms.Label();
            this.no = new System.Windows.Forms.Button();
            this.enterPrice = new System.Windows.Forms.TextBox();
            this.thankYou = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelPrompt
            // 
            this.labelPrompt.AutoSize = true;
            this.labelPrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrompt.Location = new System.Drawing.Point(13, 13);
            this.labelPrompt.Name = "labelPrompt";
            this.labelPrompt.Size = new System.Drawing.Size(0, 20);
            this.labelPrompt.TabIndex = 0;
            // 
            // buttonAddItem
            // 
            this.buttonAddItem.Location = new System.Drawing.Point(17, 96);
            this.buttonAddItem.Name = "buttonAddItem";
            this.buttonAddItem.Size = new System.Drawing.Size(131, 23);
            this.buttonAddItem.TabIndex = 2;
            this.buttonAddItem.Text = "Add value to total";
            this.buttonAddItem.UseVisualStyleBackColor = true;
            this.buttonAddItem.Click += new System.EventHandler(this.buttonAddItem_Click);
            // 
            // result
            // 
            this.result.AutoSize = true;
            this.result.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.result.Location = new System.Drawing.Point(17, 206);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(0, 16);
            this.result.TabIndex = 4;
            // 
            // labelPrompt2
            // 
            this.labelPrompt2.AllowDrop = true;
            this.labelPrompt2.AutoSize = true;
            this.labelPrompt2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPrompt2.Location = new System.Drawing.Point(14, 122);
            this.labelPrompt2.MaximumSize = new System.Drawing.Size(0, 100);
            this.labelPrompt2.Name = "labelPrompt2";
            this.labelPrompt2.Size = new System.Drawing.Size(45, 16);
            this.labelPrompt2.TabIndex = 6;
            this.labelPrompt2.Text = "label1";
            // 
            // currentTotal
            // 
            this.currentTotal.AutoSize = true;
            this.currentTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentTotal.Location = new System.Drawing.Point(168, 55);
            this.currentTotal.Name = "currentTotal";
            this.currentTotal.Size = new System.Drawing.Size(0, 16);
            this.currentTotal.TabIndex = 7;
            // 
            // no
            // 
            this.no.Location = new System.Drawing.Point(17, 163);
            this.no.Name = "no";
            this.no.Size = new System.Drawing.Size(63, 23);
            this.no.TabIndex = 9;
            this.no.Text = "Total";
            this.no.UseVisualStyleBackColor = true;
            this.no.Click += new System.EventHandler(this.no_Click);
            // 
            // enterPrice
            // 
            this.enterPrice.Location = new System.Drawing.Point(17, 55);
            this.enterPrice.Name = "enterPrice";
            this.enterPrice.Size = new System.Drawing.Size(131, 20);
            this.enterPrice.TabIndex = 10;
            // 
            // thankYou
            // 
            this.thankYou.AutoSize = true;
            this.thankYou.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.thankYou.Location = new System.Drawing.Point(17, 238);
            this.thankYou.Name = "thankYou";
            this.thankYou.Size = new System.Drawing.Size(0, 16);
            this.thankYou.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 364);
            this.Controls.Add(this.thankYou);
            this.Controls.Add(this.enterPrice);
            this.Controls.Add(this.no);
            this.Controls.Add(this.currentTotal);
            this.Controls.Add(this.labelPrompt2);
            this.Controls.Add(this.result);
            this.Controls.Add(this.buttonAddItem);
            this.Controls.Add(this.labelPrompt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPrompt;
        private System.Windows.Forms.Button buttonAddItem;
        private System.Windows.Forms.Label result;
        private System.Windows.Forms.Label labelPrompt2;
        private System.Windows.Forms.Label currentTotal;
        private System.Windows.Forms.Button no;
        private System.Windows.Forms.TextBox enterPrice;
        private System.Windows.Forms.Label thankYou;
    }
}

