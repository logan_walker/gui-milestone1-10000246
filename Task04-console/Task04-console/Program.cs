﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task04_console
{
    class Program
    {

        static string q1 = "Please type in the name of a fruit or vegetable and I will check whether or not we sell it.\n";
        static string q2 = "\nWhile you're here, would you like me to check another fruit or veg item?\n[0]Yes\n[1]No\nThen press <Enter>";
        static string q3 = "Come back any time to check for more fruit/veg items.\n\nPress <ENTER> to exit.";

        static void Main(string[] args)
        {
            while(true)
            {
                var checkDictionary = "";
                int checkAnother;
                Console.Clear();

                Console.WriteLine(q1);
                //User types in the fruir they wish to check for.
                checkDictionary = Console.ReadLine();

                Console.WriteLine(fruitVegDictionary(checkDictionary));
                Console.ReadKey();
                Console.WriteLine(q2);
                checkAnother = Convert.ToInt32(Console.ReadLine());

                if (checkAnother == 0)
                {
                    //Continues loop
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine(q3);
                    Console.ReadKey();
                    break;
                }
            }
        }

        static string fruitVegDictionary(string checkDictionary)
        {
            var yesNo = "";

            var stock = new Dictionary<string, string>();

            //Adding 10 veges to the dictionary
            stock.Add("potato", "vegetable");
            stock.Add("onion", "vegetable");
            stock.Add("carrot", "vegetable");
            stock.Add("cabbage", "vegetable");
            stock.Add("lettuce", "vegetable");
            stock.Add("broccoli", "vegetable");
            stock.Add("cauliflower", "vegetable");
            stock.Add("cucumber", "vegetable");
            stock.Add("beetroot", "vegetable");
            stock.Add("zucchini", "vegetable");
            //Also adding the plurals of the 10 veges
            stock.Add("potatoes", "vegetable");
            stock.Add("onions", "vegetable");
            stock.Add("carrots", "vegetable");
            stock.Add("cabbages", "vegetable");
            stock.Add("lettuces", "vegetable");
            stock.Add("cucumbers", "vegetable");
            stock.Add("beetroots", "vegetable");
            stock.Add("zucchinis", "vegetable");

            //Adding 10 fruit to the dictionary
            stock.Add("apple", "fruit");
            stock.Add("orange", "fruit");
            stock.Add("grape", "fruit");
            stock.Add("pear", "fruit");
            stock.Add("watermelon", "fruit");
            stock.Add("strawberry", "fruit");
            stock.Add("mandarin", "fruit");
            stock.Add("grapefruit", "fruit");
            stock.Add("papaya", "fruit");
            stock.Add("pitaya", "fruit");
            //Also adding the plurals of the 10 fruit
            stock.Add("apples", "fruit");
            stock.Add("oranges", "fruit");
            stock.Add("grapes", "fruit");
            stock.Add("pears", "fruit");
            stock.Add("watermelons", "fruit");
            stock.Add("strawberries", "fruit");
            stock.Add("mandarins", "fruit");
            stock.Add("grapefruits", "fruit");
            stock.Add("papayas", "fruit");
            stock.Add("pitayas", "fruit");

            if (stock.ContainsKey(checkDictionary))
            {
                yesNo = $"\nYes, we do indeed sell {checkDictionary}!";
            }
            else
            {
                yesNo = $"\nNo, i'm afraid we do not sell {checkDictionary}.";
            }

            return yesNo;
        }
    }
}
