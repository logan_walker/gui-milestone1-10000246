﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task02_console
{
    class Program
    {
        static string pricetest = "";
        static double price = 0.00;
        static double totalprice = 0.00;

        static string q1 = $"Enter the price of the item in dollars:";
        static string q2 = $"\nDo you want to add another item [Press 0] or check the total price (inc GST) [Press 1]";

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine(q1);
                pricetest = Console.ReadLine();
                if (Double.TryParse(pricetest, out price))
                {
                    //If this test show that user inputted a valid value program will continue as normal
                }
                else
                {
                    //If the user inputs an invalid value (not a number) this message will show
                    //program will then continue as normal and allow user to try again
                    Console.WriteLine("Enter a valid price next time.");
                }

                Console.WriteLine(q2);
                var choice = int.Parse(Console.ReadLine());
                
                Console.WriteLine($"{addtotalprice(price, choice)}");
                if (choice == 0)
                {
                    Console.Clear();
                    Console.WriteLine($"Your current total is: ${totalprice}\n");
                }
                else if (choice == 1)
                {
                    Console.WriteLine("\nThank you for using my price checker! Have a nice day.");
                    break;
                }
            }
            Console.ReadKey();
        }
        
        //Method for adding the total price and then displaying the total, depending on what option the user chooses
        static string addtotalprice(double price, int choice)
        {
            var result = "";

            switch (choice)
            {
                case 0:
                    totalprice = totalprice + price;
                    totalprice = System.Math.Round(totalprice, 2);
                    break;

                case 1:
                    totalprice = totalprice + price;
                    totalprice = System.Math.Round(totalprice, 2);
                    Console.Clear();
                    result = $"The total price (including GST) is ${Gst(totalprice)}";
                    break;

                default:
                    result = $"Not a valid value was given";
                    break;
            }
            return result;

        }

        //Method for taking the total and then returning the total+GST
        static string Gst(double totalprice)
        {
            var output = "";
            totalprice = totalprice * 1.15;

            totalprice = System.Math.Round(totalprice, 2);
            output = Convert.ToString(totalprice);

            return output;
        }
    }
}
