﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task02_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();

            labelPrompt.Text = q1;
            labelPrompt2.Text = q2;
        }

        static double price = 0.00;
        static double totalprice = 0.00;

        static string q1 = $"Enter the price of the item in dollars:";
        static string q2 = $"\nClick 'Total' when finished to display your total price (inc GST).";

        static int choice = 0;

        private void buttonAddItem_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(enterPrice.Text, out price);
            result.Text = addtotalprice(price, choice);
            totalprice = System.Math.Round(totalprice, 2);
            currentTotal.Text = $"Your current total is:\n${totalprice}";
            enterPrice.Text = String.Empty;
        }

        private void no_Click(object sender, RoutedEventArgs e)
        {
            choice = 1;
            result.Text = addtotalprice(totalprice, choice);
            thankYou.Text = "Thank you for using my price checker! Have a nice day.";
            //Freezes both buttons when total (inc GST) is shown.
            no.IsEnabled = false;
            buttonAddItem.IsEnabled = false;
        }

        //Method for adding the total price and then displaying the total, depending on what option the user chooses
        static string addtotalprice(double price, int choice)
        {
            var result = "";

            switch (choice)
            {
                case 0:
                    totalprice = totalprice + price;
                    totalprice = System.Math.Round(totalprice, 2);
                    break;

                case 1:
                    //totalprice = totalprice + price;
                    totalprice = System.Math.Round(totalprice, 2);
                    result = $"The total price (including GST) is ${Gst(totalprice)}";
                    break;

                default:
                    result = $"Please give a valid price.";
                    break;
            }
            return result;

        }

        //Method for taking the total and then returning the total+GST
        static string Gst(double totalprice)
        {
            var output = "";
            totalprice = totalprice * 1.15;

            totalprice = System.Math.Round(totalprice, 2);
            output = Convert.ToString(totalprice);

            return output;
        }
    }
}
