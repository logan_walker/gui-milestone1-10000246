﻿namespace Task04_wf
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPrompt1 = new System.Windows.Forms.Label();
            this.checkDictionary = new System.Windows.Forms.TextBox();
            this.buttonCheck = new System.Windows.Forms.Button();
            this.yesNo = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblPrompt1
            // 
            this.lblPrompt1.AutoSize = true;
            this.lblPrompt1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrompt1.Location = new System.Drawing.Point(13, 13);
            this.lblPrompt1.MaximumSize = new System.Drawing.Size(300, 0);
            this.lblPrompt1.Name = "lblPrompt1";
            this.lblPrompt1.Size = new System.Drawing.Size(45, 16);
            this.lblPrompt1.TabIndex = 0;
            this.lblPrompt1.Text = "label1";
            // 
            // checkDictionary
            // 
            this.checkDictionary.Location = new System.Drawing.Point(16, 58);
            this.checkDictionary.Name = "checkDictionary";
            this.checkDictionary.Size = new System.Drawing.Size(133, 20);
            this.checkDictionary.TabIndex = 1;
            // 
            // buttonCheck
            // 
            this.buttonCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCheck.Location = new System.Drawing.Point(16, 85);
            this.buttonCheck.Name = "buttonCheck";
            this.buttonCheck.Size = new System.Drawing.Size(133, 25);
            this.buttonCheck.TabIndex = 2;
            this.buttonCheck.Text = "Check";
            this.buttonCheck.UseVisualStyleBackColor = true;
            this.buttonCheck.Click += new System.EventHandler(this.buttonCheck_Click);
            // 
            // yesNo
            // 
            this.yesNo.AutoSize = true;
            this.yesNo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.yesNo.Location = new System.Drawing.Point(16, 117);
            this.yesNo.Name = "yesNo";
            this.yesNo.Size = new System.Drawing.Size(0, 16);
            this.yesNo.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(431, 323);
            this.Controls.Add(this.yesNo);
            this.Controls.Add(this.buttonCheck);
            this.Controls.Add(this.checkDictionary);
            this.Controls.Add(this.lblPrompt1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPrompt1;
        private System.Windows.Forms.TextBox checkDictionary;
        private System.Windows.Forms.Button buttonCheck;
        private System.Windows.Forms.Label yesNo;
    }
}

