﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task05_console
{
    class Program
    {
        static string rules1 = "Rules:";
        static string rules2 = "I will choose a random number between 1 and 5 and you have to guess what it is! Simple, right?";
        static string rules3 = "If you guess correctly you will get a point. If you guess incorrectly you will not get any points.";
        static string rules4 = "You have 5 tries to get as many points as you can! Good luck!";
        static string start = "\nI have chosen a number between 1 and 5. Can you guess it? <Guess a number between 1 and 5>";
        static string correct = "\nWell done! You guessed correctly!";
        static string incorrect = "\nBad luck. You didn't guess the right number.";
        static string invalid = "\nThat is an invalid guess. You wasted a turn. Please guess a number between 1 and 5.";
        static string result = "\nThat's terrible! You can do better!";
        static string result1 = "\nThat's not bad! You can still do better!";
        static string result2 = "\nThat's a perfect score! Well done!";
        static string playAgain = "\nWould you like to play again? \n[0] To play again\n[1] To exit game";
        static string cont = "\nPress <Enter> to continue";

        static int yesNo = 2;
        static string guessNum = "";
        static int guess = 0;
        static int number = 0;
        static int turns = 0;
        static int score = 0;
        static int turnsRemaining = 5;

        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine(rules1);
                Console.WriteLine(rules2);
                Console.WriteLine(rules3);
                Console.WriteLine(rules4);
                if (yesNo == 0)
                {
                    yesNo = 2;
                    guessNum = "";
                    guess = 0;
                    number = 0;
                    turns = 0;
                    score = 0;
                    turnsRemaining = 5;
                }
                else if (yesNo == 1)
                {
                    break;
                }
                Console.WriteLine($"\nYou have {turnsRemaining} turns left.");
                Console.WriteLine($"\nYour current score is: {score}");
                Console.WriteLine(start);
                game();
            }
        }

        //Method to generate a random number between 1 and 5 and return it
        public static int generateRandomNumber()
        {
            Random random = new Random();
            int number = random.Next(1, 6);

            return number;
        }

        //Method that contains the main game code
        public static void game()
        {
            guessNum = Console.ReadLine();
            int.TryParse(guessNum, out guess);
            number = generateRandomNumber();
            if (guess > 5 || guess < 1)
            {
                turnsRemaining = turnsRemaining - 1;
                Console.WriteLine(invalid);
                Console.WriteLine(cont);
                Console.ReadKey();
                Console.Clear();
            }
            else if (guess == number)
            {
                turnsRemaining = turnsRemaining - 1;
                score = score + 1;
                Console.WriteLine(correct);
                Console.WriteLine(cont);
                Console.ReadKey();
                Console.Clear();
            }
            else if (guess != number && guess <= 5 && guess >= 1)
            {
                turnsRemaining = turnsRemaining - 1;
                Console.WriteLine(incorrect);
                Console.WriteLine($"I guessed the number {number}");
                Console.WriteLine(cont);
                Console.ReadKey();
                Console.Clear();
            }
            turns++;
            if (turns == 5)
            {
                end();
            }
        }

        //Method that runs when the game ends. Displaying the final score
        //Also displays buttons which give the user the option to play again or exit
        public static void end()
        {
            Console.Clear();
            Console.WriteLine($"\nYour final score is: {score}");
            if (score == 0)
            {
                Console.WriteLine(result);
            }
            else if (score >= 1 && score <= 4)
            {
                Console.WriteLine(result1);
            }
            else if (score == 5)
            {
                Console.WriteLine(result2);
            }
            Console.WriteLine(playAgain);
            yesNo = Convert.ToInt32(Console.ReadLine());
        }
    }
}
