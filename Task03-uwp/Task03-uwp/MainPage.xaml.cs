﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task03_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        static string q1 = "Please select which animal you would like to learn about.";

        public MainPage()
        {
            this.InitializeComponent();
            lblPrompt1.Text = q1;
        }

        private void userOptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            output.Text = options(userOptions.SelectedIndex);
        }

        static string options(int userOptions)
        {
            var result = "";

            switch (userOptions)
            {
                case 0:
                    result = "Harambe was a Gorilla.";
                    break;
                case 1:
                    result = "Elephants are really big and have big ears.";
                    break;
                case 2:
                    result = "Lions eat things regularly.";
                    break;
                case 3:
                    result = "Giraffes have pretty long necks.";
                    break;
                default:
                    break;
            }
            return result;
        }
    }
}
